using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using TaskManagement.Models;
using TaskManagement.Repositories;

namespace TaskManagement.Pages.Task
{
    [Authorize(Roles = "Admin")]
    public class ListTasks : PageModel
    {
        private readonly ILogger<ManageTasks> _logger;

        private readonly ITaskRepository _taskRepository;
        public ListTasks(ILogger<ManageTasks> logger, ITaskRepository taskRepository)
        {
            _logger = logger;
            _taskRepository = taskRepository;
        }

        [BindProperty]
        public List<Models.Task> Tasks { get; set; }
        public void OnGetSync()
        {
            Tasks = _taskRepository.GetAllTasks();
        }
    }
}