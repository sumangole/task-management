using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using TaskManagement.Repositories;

namespace TaskManagement.Pages.Task
{
    [Authorize(Roles = "Admin")]
    public class ManageTasks : PageModel
    {
        private readonly ILogger<ManageTasks> _logger;

        private readonly ITaskRepository _taskRepository;
        public ManageTasks(ILogger<ManageTasks> logger, ITaskRepository taskRepository)
        {
            _logger = logger;
            _taskRepository = taskRepository;
        }

        [BindProperty]
        public Models.Task Task { get; set; }
        public IActionResult OnPost()
        {

            if (ModelState.IsValid)
            {
                _taskRepository.AddTask(Task);


            }
            return RedirectToPage("/Index");
        }
    }
}