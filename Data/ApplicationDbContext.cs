﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TaskManagement.Models;

namespace TaskManagement.Data;

public class ApplicationDbContext : IdentityDbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Models.Task> Tasks { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.Entity<IdentityRole>().HasData(new IdentityRole { Name = "Admin", NormalizedName = "Admin".ToUpper() });
        builder.Entity<Models.Task>()
            .HasOne(t => t.AssignedToUser)
            .WithMany(u => u.AssignedTasks)
            .HasForeignKey(t => t.AssignedToUserID)
            .OnDelete(DeleteBehavior.Restrict);

        builder.Entity<Models.Task>()
            .HasOne(t => t.AssignedByUser)
            .WithMany(u => u.CreatedTasks)
            .HasForeignKey(t => t.AssignedByUserID)
            .OnDelete(DeleteBehavior.Restrict);

        this.SeedUsers(builder);
        // this.SeedRoles(builder);
        // this.SeedUserRoles(builder);
    }

    private void SeedUsers(ModelBuilder builder)
    {
        User user = new User()
        {
            Id = "b74ddd14-6340-4840-95c2-db12554843e5",
            UserName = "Admin",
            Email = "admin@gmail.com",
            LockoutEnabled = false,
            PhoneNumber = "1234567890"
        };

        PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
        passwordHasher.HashPassword(user, "Admin*123");

        builder.Entity<User>().HasData(user);
    }
}
