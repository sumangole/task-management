using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagement.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public bool IsCompleted { get; set; }
        public string AssignedToUserID { get; set; }
        public string AssignedByUserID { get; set; }

        public User AssignedToUser { get; set; }
        public User AssignedByUser { get; set; }
    }

}