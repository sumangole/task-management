using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TaskManagement.Models
{
    public class User : IdentityUser
    {
        public List<Task> AssignedTasks { get; set; }
        public List<Task> CreatedTasks { get; set; }
        
    }
}