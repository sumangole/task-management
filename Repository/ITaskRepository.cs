using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagement.Repositories
{
    public interface ITaskRepository
    {
        Models.Task GetTaskById(int taskId);
        List<Models.Task> GetAllTasks();
        List<Models.Task> GetUserSpecificTasks(string userId);
        void AddTask(Models.Task task);
        void UpdateTask(Models.Task task);
        void DeleteTask(int taskId);
    }
}