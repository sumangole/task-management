using System.Collections.Generic;
using System.Linq;
using TaskManagement.Data;
using TaskManagement.Models;

namespace TaskManagement.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly ApplicationDbContext dbContext;

        public TaskRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Models.Task GetTaskById(int taskId)
        {
            return dbContext.Tasks.FirstOrDefault(t => t.Id == taskId);
        }

        public List<Models.Task> GetAllTasks()
        {
            return dbContext.Tasks.ToList();
        }

        public void AddTask(Models.Task task)
        {
            dbContext.Tasks.Add(task);
            dbContext.SaveChanges();
        }

        public void UpdateTask(Models.Task task)
        {
            dbContext.Tasks.Update(task);
            dbContext.SaveChanges();
        }

        public void DeleteTask(int taskId)
        {
            Models.Task task = dbContext.Tasks.FirstOrDefault(t => t.Id == taskId);
            if (task != null)
            {
                dbContext.Tasks.Remove(task);
                dbContext.SaveChanges();
            }
        }
        public List<Models.Task> GetUserSpecificTasks(string userId)
        {
            return dbContext.Tasks
               .Where(t => t.AssignedToUserID == userId)
               .ToList();
        }
    }
}
